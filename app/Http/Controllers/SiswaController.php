<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function index()
    {
        $siswa = Siswa::all();
        return view('siswa.index', compact('siswa'));
    }

    public function create(Request $request)
    {
        return view('siswa.create');
    }

    public function store(Request $request)
    {
       $siswa = new Siswa;
       $siswa->name = $request->name;
       $siswa->nis = $request->nis;
       $siswa->gender = $request->gender;
       $siswa->tanggal_lahir = $request->tanggal_lahir;
       $siswa->save();
       return redirect()->route('siswa.index');
    }

    public function show($id)
    {
     
      $siswa = Siswa::with('pinjam.boroRef')->findOrFail($id);
       return view('siswa.history', compact('siswa'));
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('siswa.edit', compact('siswa'));
    }

    public function update(Request $request, $id)
    {
       $siswa =Siswa::findOrFail($id);
       $siswa->name = $request->name;
       $siswa->nis = $request->nis;
       $siswa->gender = $request->gender;
       $siswa->tanggal_lahir = $request->tanggal_lahir;
       $siswa->update();
       return redirect()->route('siswa.index');
    }

    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();
        return redirect()->route('siswa.index');
    }
}
