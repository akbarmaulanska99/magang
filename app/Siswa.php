<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['name', 'nis', 'gender', 'tanggal_lahir'];
    //protected $guarded = ['id'];

    public function pinjam()
    {
    	return $this->hasMany(Borrow::class, 'siswa_id');
    }
}
