@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Kategori</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li>
                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
                <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('book_category.update', $category->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" 
                            id="name" placeholder="Masukkan Nama" value="{{ $category->name }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea class="form-control" name="description" 
                            id="description" rows="4">{{ $category->description }}</textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
@section('content2')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ubah Data Kategori</div>
                    <div class="card-body">
                        <form action="{{ route('book_category.update', $category->id) }}" method="post">
                            @csrf
                            @method('put')
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Nama</label>
                                            <input type="text" name="name" class="form-control" 
                                            id="name" placeholder="Masukkan Nama" value="{{ $category->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Deskripsi</label>
                                            <textarea class="form-control" name="description" 
                                            id="description" rows="4">{{ $category->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-danger">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
