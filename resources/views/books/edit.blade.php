@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Buku</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('books.update', $book->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="siswa'">Judul Buku</label>
                          <input type="text" name="title" class="form-control" id="title" placeholder="Masukan Nama"
                           value="{{ $book->title }}">
                    </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="category">Kategori</label>
                            <select class="form-control" id="books" name="category" id="category">
                                <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}" {{ $book->book_category_id ==
                             $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                                @endforeach
                            </select>

                    </div>
                    <div class="form-group col-md-6">
                        <label for="author">Penulis</label>
                        <input type="text" name="author" class="form-control" id="author" 
                        value="{{ $book->author }}" placeholder="Masukan Nama">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="publisher">Penerbit</label>
                        <input type="text" name="publisher" class="form-control" id="publisher" 
                        value="{{ $book->publisher }}" placeholder="Masukan Nama">
                    </div>
                     <div class="form-group col-md-6">
                        <label for="year">Tahun</label>
                        <input type="text" class="form-control" id="year" 
                        name="year" placeholder="Masukan Tahun" value="{{ $book->year }}">
                    </div>
                   
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
