@extends('layouts.template')

@section('content')
<div class="col-md-12 col-sm-6">
  <div class="row">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data Buku</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
              <a href="{{ route('books.create') }}">
                <button type="button" class="btn btn-primary btn-sm">Tambah</button>
              </a>
            </li>
            <li>
              <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr style="text-align: center;">
                <th scope="col">No</th>
                <th scope="col">Judul Buku</th>
                <th scope="col">Kategori</th>
                <th scope="col">Penulis</th>
                <th scope="col">Penerbit</th>
                <th scope="col">Tahun</th>
                <th scope="col">Pilihan</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($books as $i => $item)
            <tr style="text-align: center;">
                <td>{{ $i+1 }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->categoryRef->name }}</td>
                <td>{{ $item->author }}</td>
                <td>{{ $item->publisher }}</td>
                <td>{{ $item->year }}</td>
                <td>
                  <form action="{{ route('books.destroy',$item->id) }}" method="post">
                  <a href="{{ route('books.edit',$item->id) }}">
                    <button type="button" class="btn btn-success">Ubah</button>
                  </a>
                  @csrf 
                  @method('delete')
                    <button type="submit" class="btn btn-danger" 
                    onclick="return confirm('Yakin ingin dihapus ?')">Hapus</button>
                    <a href="{{ route('books.show',$item->id) }}">
                       <button type="button" class="btn btn-primary">Detail</button>
                       </a>
                  </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div> 
@endsection
