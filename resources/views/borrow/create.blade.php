@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Tambah Data Peminjaman</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('borrow.store') }}" method="post">
            @csrf
                <div class="form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="siswa'">siswa</label>
                          <select class="form-control" id="siswa" name="siswa">
                                <option>chose...</option>
                                @foreach ($siswa as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>

                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Judul Buku</label>
                            <select class="form-control" id="books" name="books">
                                <option>chose...</option>
                                @foreach ($books as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>

                    </div>
                    <div class="form-group col-md-12">
                        <label for="tanggal_pinjam">Tanggal pinjam</label>
                        <input type="date" class="form-control" id="tanggal_pinjam" 
                        name="tanggal_pinjam" placeholder="Masukan Tanggal">
                    </div>
                    
                    </div>
                <button type="submit" class="btn btn-danger">Pinjam</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection

@push('script')
<script>
    console.log('tes')
</script>
@endpush