@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Peminjaman</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('borrow.update', $borrow->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="siswa'">siswa</label>
                          <select class="form-control" id="siswa" name="siswa">
                                <option>chose...</option>
                                @foreach ($siswa as $item)
                                <option value="{{ $item->id }}"{{ $borrow->siswa_id ==
                             $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                                @endforeach
                            </select>

                      </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Judul Buku</label>
                            <select class="form-control" id="books" name="books">
                                <option>chose...</option>
                                @foreach ($books as $item)
                                <option value="{{ $item->id }}" {{ $borrow->book_id ==
                             $item->id ? 'selected' : ''}}>{{ $item->title }}</option>
                                @endforeach
                            </select>

                    </div>
                    <div class="form-group col-md-6">
                        <label for="tanggal_pinjam">Tanggal pinjam</label>
                        <input type="date" class="form-control" id="tanggal_pinjam" 
                        name="tanggal_pinjam" value="{{ $borrow->tanggal_pinjam }}" placeholder="Masukan Tanggal">
                    </div>
                    </div>

                     <div class="form-group col-md-6">
                        <label for="tanggal_pinjam">Tanggal Kembali <small id="kembali"></small></label>
                        <input type="date" class="form-control" id="tanggal_kembali" 
                        name="tanggal_kembali">
                    </div>
                    </div>

                    <label class="mr-4">Status</label><br>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status" id="status1" value="dipinjam" class="custom-control-input"
                     {{ $borrow->status == 'dipinjam' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="status1">Dipinjam</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status" id="status2" value="dikembalikan" class="custom-control-input"
                     {{ $borrow->status == 'dikembalikan' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="status2">Dikembalikan</label>
                </div>
                 </div>

                    <div class="form-group col-md-12">
                        <label for="category">Denda</label>
                            <input type="text" class="form-control" id="denda" 
                        name="denda" readonly>
                                
                            
                    
                    </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection

@push('script')
<script>
    $('#tanggal_pinjam, #tanggal_kembali').change(()=>{

        var pinjam = $('#tanggal_pinjam').val()
        var kembali = $('#tanggal_kembali').val()
        //cek jika input pinjam dan kembali tidak kosong

        if ( Date.parse(pinjam) && Date.parse(kembali) )
        {
            pinjam = new Date(pinjam)
            kembali = new Date(kembali)

            //membuat tanggal deadline
            var deadline = pinjam.setDate(pinjam.getDate() + 7)

            //menghitung selisih hari dan denda
            var selisih = (kembali - deadline) / (1000 * 3600 * 24)
            var denda = selisih > 0 ? (selisih*500) : 0;

            $('#denda').val(denda)

            //membuat fungsi data  tanggal deadline untuk ditampilkan
            var convertDeadline = new Date(deadline)
            var formattedDate = ('0' + convertDeadline.getDate()).slince(-2);
            var formattedMonth = ('0' + (convertDeadline.getMonth() + 1)).slince(-2);
            var formattedYear = convertDeadline.getFullYear().tostring().substr(2,2);
            var dateString = formattedMonth + '/' + formattedDate + '/' + formattedYear;

            //menampilkan deadline
            $('#kembali').text( '(Deadline : '+dateString+' )' )
        }
        else{
            $('#kembali').text( '(Deadline : - )' )
            $('#denda').val(0)
        
        } 
    })

</script>
@endpush
