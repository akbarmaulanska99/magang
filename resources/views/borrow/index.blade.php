@extends('layouts.template')

@section('content')
<div class="col-md-12 col-sm-6">
  <div class="row">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data Peminjaman</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
              <a href="{{ route('borrow.create') }}">
                <button type="button" class="btn btn-primary btn-sm">Tambah</button>
              </a>
            </li>
            <li>
              <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr style="text-align: center;">
                <th scope="col">id</th>
                <th scope="col">Siswa</th>
                <th scope="col">Book</th>
                <th scope="col">Tanggal Pinjam</th>
                <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($borrow as $i => $item)
            <tr style="text-align: center;"> 
                <td>{{ $i+1 }}</td>
                <td>{{ $item->borosRef->name }}</td>
                <td>{{ $item->boroRef->title }}</td>
                <td>{{ $item->tanggal_pinjam }}</td>
                <td>{{ $item->status }}</td>
                <td>
                 <form action="{{ route('borrow.destroy',$item->id) }}" method="post">
                  <a href="{{ route('borrow.edit',$item->id) }}">
                    <button type="button" class="btn btn-success">Ubah</button>
                  </a>
                  @csrf 
                  @method('delete')
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin ingin dihapus ?' )">Hapus</button>
                  </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>  
@endsection