@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Siswa</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li>
                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li>
                <a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ url('/siswa/update/'.$siswa->id) }}" method="post">
            @csrf
            @method('put')
            <label for="name"> Name * </label>
                <input type="text" id="name" class="form-control" name="name" value="{{ $siswa->name }}">
            <br>
            <label for="nis">NIS * </label>
                <input type="number" id="nis" class="form-control" name="nis" value="{{ $siswa->nis }}">
            <br>
            <label class="mr-4">Gender *</label><br>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="gender" id="gender1" value="laki-laki" class="custom-control-input" {{ $siswa->gender == 'laki-laki' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="gender1">Laki-Laki</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="gender" id="gender2" value="perempuan" class="custom-control-input" {{ $siswa->gender == 'perempuan' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="gender2">Perempuan</label>
                </div>
            <br>
            <br>
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir *</label>
                <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" value=" {{ $siswa->tanggal_lahir }}">
            </div>

            <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection

