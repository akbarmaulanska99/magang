@extends('layouts.template')

@section('content')
 <!-- page content -->
     
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Profil Dan Histori Peminjaman Buku</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3  profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{{ asset('images/index.jpg') }}" style="width: 120px; height: 120px;" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>Bio Siswa</h3>

                      <ul class="list-unstyled user_data">
                        <li>
                          Nama Siswa :  {{ $siswa->name }}
                        </li>

                        <li>
                          Nis Siswa   :  {{ $siswa->nis }}
                        </li>

                        <li>
                          Gender      :   {{ $siswa->gender }}
                        </li>

                         <li>
                          Tanggal Lahir      :   {{ $siswa->tanggal_lahir }}
                        </li>
                      </ul>

                     </div> 
                    <div class="col-md-9 col-sm-9 ">
                      <div class="table">
                          <table class="data table table-striped no-margin">
                              <thead>
                                <tr style="text-align: center;">
                                  <th>No</th>
                                  <th>Judul Buku</th>
                                  <th>Tanggal Pinjam </th>
                                  <th>Tanggal Kembali</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($siswa->pinjam as $i => $item)
                                <tr style="text-align: center;">
                                  <td>{{ $i+1 }}</td>
                                  <td>{{ $item->boroRef->title }}</td>
                                  <td>{{ $item->tanggal_pinjam }}</td>
                                  <td>{{ $item->tanggal_kembali }}</td>
                              </tr>
                               @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection