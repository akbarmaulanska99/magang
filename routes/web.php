<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::get('/tes', function () {
 	return view('tes');
 });

Route::get('/', function () {
    return view('welcome');
});

 Route::group(['middleware' => ['auth']], function () {
 	Route::get('/home', 'HomeController@index')->name('home');



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/siswa', 'SiswaController@index')->name('siswa.index');
Route::get('/siswa/edit/{id}', 'SiswaController@edit')->name('siswa.edit');
Route::get('/siswa/history/{id}', 'SiswaController@show')->name('siswa.show');
Route::get('/siswa/tambah', 'SiswaController@create')->name('siswa.create');


Route::post('/siswa/store', 'SiswaController@store')->name('siswa.store');
Route::put('/siswa/update/{id}', 'SiswaController@update')->name('siswa.update');
Route::delete('/siswa/delete/{id}', 'SiswaController@destroy')->name('siswa.destroy');

Route::resources([
	'book_category' => 'BookCategoryController',
    'books' => 'BookController',
    'borrow'=>'BorrowController'
]);


});

Auth::routes();